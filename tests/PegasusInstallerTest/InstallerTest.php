<?php
/**
 * pegasus-composer-installer
 *
 * @author  keny.lieou
 * @package InstallerTest.php
 * @license Contemi License
 * @version 1.0
 * @since   08/04/2016 6:39 PM
 *
 */
namespace PegasusInstallerTest;

use Composer\Package\Loader\ArrayLoader;
use Composer\Script\Event;
use Composer\Test\Installer\LibraryInstallerTest;
use Pegasus\ComposerPlugin\Installer\PegasusInstaller;
use Pegasus\ComposerPlugin\Plugin;

/**
 * Tests registering Pegasus Installer with Composer.
 */
class InstallerTest extends LibraryInstallerTest
{

    /**
     * @var \Composer\Config
     */
    protected $config;

    /**
     * Test the Installer's support() function.
     *
     * @param $type
     *   The type of library.
     * @param $expected
     *   Whether or not the given type is supported by Pegasus Installer.
     *
     * @return void
     *
     * @dataProvider providerPegasusSupports
     */
    public function testPegasusSupports($type, $expected)
    {
        $installer = new PegasusInstaller($this->io, $this->composer, 'pegasus');
        $this->assertSame($expected, $installer->supports($type), sprintf('Failed to show support for %s', $type));
    }

    /**
     * Data provider for testPegasusSupports().
     *
     * @see testPegasusSupports()
     */
    public function providerPegasusSupports()
    {
        // All package types support having Pegasus.
        $tests[] = array('pegasus', true);
        $tests[] = array('all-supported', false);
        return $tests;
    }

    /**
     * Tests the Installer's getPegasusPath function.
     *
     * @param $expected
     *   The expected install path for the package.
     * @param $package
     *   The package to test upon.
     *
     * @dataProvider providerGetPegasusPath
     */
    public function testGetPegasusPath($expected, $package)
    {
        // Fix for window
        $expected = str_replace('/', DIRECTORY_SEPARATOR, $expected);
        // Construct the mock objects.
        $installer = new PegasusInstaller($this->io, $this->composer, 'pegasus');
        $loader = new ArrayLoader();
        // Test the results.
        $result = $installer->getInstallPath($loader->load($package));
        $this->assertEquals($expected, $result);
    }

    /**
     * Data provider for testGetPegasusPath().
     *
     * @see testGetPegasusPath()
     */
    public function providerGetPegasusPath()
    {
        $package = array(
            'name' => 'foo/bar',
            'type' => 'pegasus-web',
            'version' => '1.0.0',
        );
        $tests[] = array('bar', $package);

        $package = array(
            'name' => 'foo/bar2',
            'type' => 'pegasus-service',
            'version' => '1.0.0',
        );
        $tests[] = array('bar2', $package);

        $package = array(
            'name' => 'foo/bar3',
            'type' => 'pegasus-common',
            'version' => '1.0.0',
        );
        $tests[] = array('common-share/bar3', $package);

        $package = array(
            'name' => 'foo/bar4',
            'type' => 'pegasus-share',
            'version' => '1.0.0',
        );
        $tests[] = array('common-share/bar4', $package);

        $package = array(
            'name' => 'foo/bar5',
            'type' => 'pegasus-test',
            'version' => '1.0.0',
        );
        $tests[] = array('test/bar5', $package);

        return $tests;
    }

    /**
     * Test git hook
     */
    public function testGitHook()
    {
        $loader = new ArrayLoader();
        $package = $loader->load(
            [
                'name' => 'contemi/githook',
                'type' => 'pegasus-test',
                'version' => 1
            ]
        );
        Plugin::setContemiPackage($package);
        $event = new Event('post-update-cmd', $this->composer, $this->io);
        Plugin::doRegisterGitHook($event);

        $installer = new PegasusInstaller($this->io, $this->composer, 'pegasus');
        $path = $installer->getInstallPath($package);

        $this->assertTrue(is_dir($path . '/.git/hooks'), "Git hooks dir not found");
        $this->assertTrue(file_exists($path . '/.git/hooks/pre-commit'), "Pre-commit hook not found");
    }

    protected function setUp()
    {
        // Run through the Library Installer Test set up.
        parent::setUp();
    }
}
