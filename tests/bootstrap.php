<?php
/**
 * pegasus-composer-installer
 *
 * @author  keny.lieou
 * @package bootstrap.php
 * @license Contemi License
 * @version 1.0
 * @since   08/04/2016 6:38 PM
 *
 */

error_reporting(E_ALL ^ E_DEPRECATED);

// Add the Component Installer test paths.
$loader = require __DIR__ . '/../src/bootstrap.php';
$loader->add('PegasusInstallerTest', __DIR__);

// Allow use of the Composer\Test namespace.
$path = $loader->findFile('Composer\\Composer');
$loader->add('Composer\Test', dirname(dirname(dirname($path))) . '/tests');
