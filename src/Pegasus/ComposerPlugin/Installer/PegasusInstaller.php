<?php
/**
 * pegasus-composer-installer
 *
 * @author  keny.lieou
 * @package PegasusInstaller.php
 * @license Contemi License
 * @version 1.0
 * @since   08/04/2016 6:09 PM
 *
 */

namespace Pegasus\ComposerPlugin\Installer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;

class PegasusInstaller extends LibraryInstaller
{
    const PEGASUS_PACKAGE_TYPE_PREFIX = 'pegasus';

    protected $types = [
        'service', 'web', 'common', 'share', 'tool', 'test', 'doc'
    ];

    protected $defaultDir = [
        'service' => '',
        'web' => '',
        'common' => 'common-share',
        'share' => 'common-share',
        'tool' => 'tool',
        'test' => 'test',
        'doc' => 'doc'
    ];

    /**
     * Get regex
     *
     * @return string
     */
    public function getRegex()
    {
        $types = $this->getTypes();
        return "/^([a-zA-Z1-9-_]+)-({$types})$/";
    }

    /**
     * Get types
     *
     * @return string
     */
    public function getTypes()
    {
        return implode('|', $this->types);
    }

    /**
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $package
     */
    public function uninstall(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        if (!$repo->hasPackage($package)) {
            throw new \InvalidArgumentException('Package is not installed: ' . $package);
        }

        $repo->removePackage($package);

        $installPath = $this->getInstallPath($package);
        $this->io->write(sprintf('Deleting %s - %s', $installPath, $this->filesystem->removeDirectory($installPath) ? '<comment>deleted</comment>' : '<error>not deleted</error>'));
    }

    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        $name = $package->getPrettyName();

        $parts = explode('/', $name);
        if (count($parts) != 2) {
            throw new \InvalidArgumentException(
                "Invalid package name [{$name}]. Should be in the form of vendor/package"
            );
        }
        return $this->getPegasusConfigDir($package);
    }

    /**
     * @param PackageInterface $package
     * @return string
     */
    public function getPegasusConfigDir(PackageInterface $package)
    {
        list(, $packageName) = explode('/', $package->getName());
        $type = $package->getType();

        $configDirKey = $type . '-dir';
        $type = substr($type, strlen(self::PEGASUS_PACKAGE_TYPE_PREFIX) + 1);
        if (!empty($type) && array_key_exists($type, $this->defaultDir)) {
            $type = $this->defaultDir[$type];
        }
        /** @var \Composer\Config $config */
        $config = $this->composer->getConfig();

        $dir = $config->has($configDirKey) ? $config->get($configDirKey) : $type;
        if (!empty($dir)) {
            return $dir . DIRECTORY_SEPARATOR . $packageName;
        }
        return $packageName;

    }

    /**
     * @param $packageType
     * @return bool
     */
    public function supports($packageType)
    {
        return strpos($packageType, self::PEGASUS_PACKAGE_TYPE_PREFIX) === 0;
    }
}
