<?php
/**
 * pegasus-composer-installer
 *
 * @author  keny.lieou
 * @package PegasusInstallerPlugin.php
 * @license Contemi License
 * @version 1.0
 * @since   08/04/2016 5:50 PM
 *
 */

namespace Pegasus\ComposerPlugin;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

use Pegasus\ComposerPlugin\Installer\PegasusInstaller;

class Plugin implements PluginInterface
{
    private static $contemiPackages = [];
    private static $runAsPlugin = false;

    protected $installers = [
        'PegasusInstaller'
    ];

    /**
     * @param Composer $composer
     * @param IOInterface $io
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        /** @var $installationManager InstallationManager */
        $installationManager = $composer->getInstallationManager();
        foreach ($this->installers as $class) {
            $installationManager->addInstaller($this->getInstaller($class, $composer, $io));
        }
    }

    /**
     * @param             $class
     * @param Composer $composer
     * @param IOInterface $io
     * @return mixed
     */
    public function getInstaller($class, Composer $composer, IOInterface $io)
    {
        $installer = __NAMESPACE__ . '\\Installer\\' . $class;
        return new $installer($io, $composer);
    }

}
