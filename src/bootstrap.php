<?php
/**
 * pegasus-composer-installer
 *
 * @author  keny.lieou
 * @package bootstrap.php
 * @license Contemi License
 * @version 1.0
 * @since   08/04/2016 6:03 PM
 *
 */

function includeIfExists($file)
{
    if (file_exists($file)) {
        /** @noinspection PhpIncludeInspection */
        defined('PEGASUS_APP_ROOT') or define('PEGASUS_APP_ROOT', dirname(__DIR__));
        defined('PEGASUS_VENDOR_DIR') or define('PEGASUS_VENDOR_DIR', dirname(realpath($file)));
        defined('PEGASUS_BIN_DIR') or define('PEGASUS_BIN_DIR', dirname(realpath($file)) . '/bin');
        return include $file;
    }
    return null;
}

$files = [];
$files[] = __DIR__ . '/../vendor/autoload.php';
$files[] = __DIR__ . '/../../../vendor/autoload.php';

$loader = null;
foreach ($files as $file) {
    if ($loader = includeIfExists($file)) {
        break;
    }
}
if (empty($loader)) {
    die('You must set up the project dependencies, run the following commands:' . PHP_EOL .
        'curl -s http://getcomposer.org/installer | php' . PHP_EOL .
        'php composer.phar install' . PHP_EOL);
}
return $loader;
